How to contribute
Bug reports and pull requests from users are what keep this project working.

Basics
Create an issue and describe your idea
Fork it
Create your feature branch (git checkout -b my-new-feature)
Commit your changes (git commit -am 'Add some feature')
Publish the branch (git push origin my-new-feature)
Create a new Pull Request
Checking your work
You can test your workflow with changelog generator with the github-changelog-generator/changelog_test repo.

You can run the test suite.

You can run RuboCop to check code style.

The default Rake task, runnable using rake, calls rubocop, then spec.

Write documentation
This project has documentation in a few places:

Introduction and usage
A friendly README.md written for many audiences.

Examples and advanced usage
The wiki.

API documentation
API documentation is written as YARD docblocks in the Ruby code.

This is rendered as Web pages on Rubydoc.info.

The completeness of the API documentation is measured on our page on the Inch CI website.

man page
man/git-generate-changelog.md

The man page is for the git generate-changelog Git sub-command, which is a wrapper for github_changelog_generator. That file is a Markdown file.

Use the ronn gem to generate .1 and .html artifacts like this: cd man; ronn git-generate-changelog.md

Releasing a new version
Clone the repository
Install the dependencies (look at the CI configuration for the most updated procedure)
Update the changelog (and commit it afterwards): github_changelog_generator -t <YOUR_GH_TOKEN> --future-release <FUTURE_RELEASE>
Update the certification authority: bundle exec rake update_ssl_ca_file
Push the changes and wait for the latest CI build to complete
Bump the version, create a Git tag and commit the changes: bundle exec bump pre --tag
Push the version bump commit: git push
Push the Git tag: git push --tags
Build the new gem: bundle exec rake build
Push the new gem: bundle exec rake release
If you get the Your rubygems.org credentials aren't set error, set your RubyGems.org credentials:

curl -u your-rubygems-username https://rubygems.org/api/v1/api_key.yaml > $HOME/.gem/credentials